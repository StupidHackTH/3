window.EVENT_INFO = {
  sponsors: {
    corporate: [
      {
        name: "React Bangkok",
        url: "https://web.facebook.com/reactbkk/",
        image: "reactbkk.png",
      },
      {
        name: "MOXGA",
        url: "https://moxga.com/",
        image: "moxga.png",
      },
      {
        name: "Bangmod.Cloud",
        url: "https://bangmod.cloud/",
        image: "bangmodcloud.png",
      },
      {
        name: "KULAP.io",
        url: "https://kulap.io/",
        image: "kulap.png",
      },
      {
        name: "Siam Chamnankit",
        url: "https://siamchamnankit.co.th/",
        image: "sck.png",
      },
      {
        name: "MANLIN.NOP",
        image: "manlin-nop.png",
      },
      {
        name: "WISESIGHT",
        url: "https://wisesight.com/",
        image: "wisesight.png",
      },
      {
        name: "Band Protocol",
        url: "https://bandprotocol.com/",
        image: "bandprotocol.png",
      },
    ],
    individual: [
      { name: "Phubet Suwanich" },
      { name: "Joey" },
      { name: "Tanawat Amp Tassana" },
      { name: "Pawit P." },
      { name: "Sasarak Sutthisukon" },
      { name: "Parin Chiamananthapong" },
      { name: "Sakul Montha" },
      { name: "Jirat Kijlerdpornpailoj" },
      { name: "Wasith Theerapattrathamrong" },
      { name: "Poohdish Rattanavijai" },
      { name: "Nualporn Smerasuta" },
      { name: "Pasita Kuku" },
      { name: "Pankamol Srikaew" },
      { name: "Tossapon Nuanchuay" },
      {
        name: "Michael Wongwaisayawan",
        url: "https://web.facebook.com/groups/techxsocial/",
      },
      { name: "Natthanat Julotok" },
      { name: "Chayaporn Tantisukarom" },
    ],
  },
}
