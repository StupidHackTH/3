import Typography from "typography"

const typography = new Typography({
  baseFontSize: "18px",
  baseLineHeight: 1.666,
  headerFontFamily: ["Karla", "sans-serif"],
  bodyFontFamily: ["Karla", "sans-serif"],
  headerColor: "#fa709a",
  bodyColor: "#e9e8e7",
  googleFonts: [
    {
      name: "Karla",
      styles: ["400", "600", "700"],
    },
  ],
})

export default typography
