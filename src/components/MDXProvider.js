// https://github.com/ChristopherBiscardi/gatsby-mdx/issues/74#issuecomment-415559415
import React from "react"
import { MDXProvider as BaseMDXProvider } from "@mdx-js/tag"

const MDXProvider = ({ children }) => (
  <BaseMDXProvider
    components={{
      wrapper: React.Fragment,
    }}
  >
    {children}
  </BaseMDXProvider>
)

export default MDXProvider
