import React from "react"
import PropTypes from "prop-types"
import { StaticQuery, graphql } from "gatsby"

import "./layout.css"

const Layout = ({ children }) => (
  <StaticQuery
    query={graphql`
      query SiteTitleQuery {
        site {
          siteMetadata {
            title
          }
        }
      }
    `}
    render={data => (
      <React.Fragment>
        <div className="bg" />
        <div style={{ position: "relative" }}>
          <div
            style={{
              margin: `0 auto`,
              maxWidth: 960,
              padding: `0px 1.0875rem 1.45rem`,
              paddingTop: 64,
            }}
          >
            <header style={{ position: "relative" }}>
              <h1
                style={{
                  color: "white",
                  mixBlendMode: "multiply",
                  fontSize: "3em",
                  maxWidth: "12em",
                }}
              >
                <span>The</span> <span>Stupid</span> <span>Hackathon</span>{" "}
                <span>Thailand</span> <span>#3</span>
              </h1>
            </header>
          </div>
          <div
            style={{
              margin: `0 auto`,
              maxWidth: 800,
              padding: `0px 2rem 5rem`,
              paddingTop: 64,
            }}
          >
            <main>{children}</main>
          </div>
        </div>
      </React.Fragment>
    )}
  />
)

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
