/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/browser-apis/
 */

import React from "react"
import MDXProvider from "./src/components/MDXProvider"

export const wrapRootElement = ({ element }) => (
  <MDXProvider>{element}</MDXProvider>
)
